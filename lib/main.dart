import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'dart:io';

void main() {
  // debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;
  runApp(MaterialApp(home: MyApp()));
}

Widget _raisedButton(BuildContext context, Color currentColor, Function function, String parts) {
  return Container(
    margin: EdgeInsets.all(16.0),
    child: Row(    // 1行目
      children: <Widget>[
        Expanded(  // 2.1列目
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              RaisedButton(
                elevation: 3.0,
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        titlePadding: const EdgeInsets.all(0.0),
                        contentPadding: const EdgeInsets.all(0.0),
                        content: SingleChildScrollView(
                          child: ColorPicker(
                            pickerColor: currentColor,
                            onColorChanged: function,
                            colorPickerWidth: 300.0,
                            pickerAreaHeightPercent: 0.7,
                            enableAlpha: true,
                            displayThumbColor: true,
                            showLabel: true,
                            paletteType: PaletteType.hsv,
                            pickerAreaBorderRadius: const BorderRadius.only(
                              topLeft: const Radius.circular(2.0),
                              topRight: const Radius.circular(2.0),
                            ),
                          ),
                        ),
                      );
                    },
                  );
                },
                child: Text(parts),
                color: currentColor,
                textColor: useWhiteForeground(currentColor)
                    ? const Color(0xffffffff)
                    : const Color(0xff000000),
              ),
            ],
          ),
        ),
        Text(currentColor.toString()),
      ],
    )
  );
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Color currentHeaderColor = Colors.limeAccent;
  Color currentBodyColor = Colors.limeAccent;
  Color currentFooterColor = Colors.limeAccent;
  List<Color> currentColors = [Colors.limeAccent, Colors.green];

  void _changeHeaderColor(Color color) => setState(() => currentHeaderColor = color);
  void _changeBodyColor(Color color) => setState(() => currentBodyColor = color);
  void _changeFooterColor(Color color) => setState(() => currentFooterColor = color);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    final bodyHeight = screenHeight - 50 - 100 - 100;

    return Theme(
      data: ThemeData.light(),
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(100.0),
            child: AppBar(
              title: Text('Flutter HP Color Designer'),
              bottom: TabBar(
                tabs: <Widget>[
                  const Tab(text: 'Color Chooser'),
                  const Tab(text: 'Preview'),
                ],
              ),
            ),
          ),
          body: TabBarView(
            physics: const NeverScrollableScrollPhysics(),
            children: <Widget>[
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  _raisedButton(context, currentHeaderColor, _changeHeaderColor, "Header"),
                  _raisedButton(context, currentBodyColor, _changeBodyColor, "Body"),
                  _raisedButton(context, currentFooterColor, _changeFooterColor, "Footer"),
                ],
              ),
              Center(
                child: Column(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(color: currentHeaderColor),
                      width: screenWidth,
                      height: 50,
                      child: Text('Header', style: TextStyle(fontSize: 20),),
                    ),
                    Container(
                      decoration: BoxDecoration(color: currentBodyColor),
                      width: screenWidth,
                      height: bodyHeight,
                      child: Text('Body', style: TextStyle(fontSize: 20),),
                    ),
                    Container(
                      decoration: BoxDecoration(color: currentFooterColor),
                      width: screenWidth,
                      height: 100,
                      child: Text('Footer', style: TextStyle(fontSize: 20),),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
